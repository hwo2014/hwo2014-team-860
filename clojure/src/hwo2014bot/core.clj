(ns hwo2014bot.core
  (:require [clojure.data.json :as json])
  (:use [aleph.tcp :only [tcp-client]]
        [lamina.core :only [enqueue wait-for-result wait-for-message]]
        [gloss.core :only [string]])
  (:gen-class))

(defn- json->clj [string]
  (json/read-str string :key-fn keyword))

(defn send-message [channel message]
  (enqueue channel (json/write-str message)))

(defn read-message [channel]
  (json->clj
    (try
      (wait-for-message channel)
      (catch Exception e
        (println (str "ERROR: " (.getMessage e)))
        (System/exit 1)))))

(defn connect-client-channel [host port]
  (wait-for-result
   (tcp-client {:host host,
                :port port,
                :frame (string :utf-8 :delimiters ["\n"])})))

(def cars (atom []))
(def my-car (atom nil))
(def pieces (atom []))
(def lanes (atom []))
(def switch-tracker (atom {}))
(def max-velocities (atom []))
(def angles (atom [0 0 0 0 0]))
(def current-velocities (atom [0 0 0 0 0]))
(def previous-positions (atom  (vec (take 6 (repeat {:angle 0.0
                                                     :piecePosition {:pieceIndex 0, :inPieceDistance 0.0,
                                                                     :lane {:startLaneIndex 0, :endLaneIndex 0},
                                                                     :lap 0 :throttle 0}})))))
(def throttle-coeff (atom nil))
(def decel-coeff (atom nil))
(def F-slip (atom nil))
(def turbo-available (atom nil))
(def turbo-active? (atom false))
(def turbo-available? (atom false))
(def race-session (atom nil))

(defn get-piece [index]
  (let [size (count @pieces)]
    (if (< index size)
      (get @pieces index)
      (get @pieces (rem index size)))))

(defn last-lap? [lap]
  (let [laps (or (:laps @race-session) 3)]
    (when (= lap (- laps 1))
      true)))

(defn get-max-v [index lane lap]
  (let [size (count @pieces)]
    (if (< index size)
      (get (get @max-velocities lane) index)
      (if (last-lap? lap)
        100
        (get (get @max-velocities lane) (rem index size))))))

(defn find-next-turn [current]
  (if (and (:angle (get-piece current))
           (:angle (get-piece (inc current)))
           (and (= (:angle (get-piece current))
                   (:angle (get-piece (inc current))))
                (= (:radius (get-piece current))
                   (:radius (get-piece (inc current))))))
    (find-next-turn (inc current))
    (if (:length (get-piece current))
      (if (:angle (get-piece (inc current)))
        (inc current)
        (find-next-turn (inc current)))
      (if (:radius (get-piece current))
        (if (:length (get-piece (inc current)))
          (find-next-turn (inc current))
          (inc current))))))

(defn find-next-switch [current]
  (if (:switch (get-piece current))
    (inc current)
    (find-next-switch (inc current))))

(defn find-next-turn-direction [next-turn]
  (let [turn-piece (get-piece next-turn)]
    (if (> (:angle turn-piece) 0) "Right" "Left")))

(defn turn? [piece]
  (:radius piece))

(defn switch? [piece]
  (:switch piece))

(defn straight? [piece]
  (:length piece))

(defn first-piece-of-turn? [piece-index]
  (let [current-piece (get-piece piece-index)
        prev-piece (get-piece (inc piece-index))]
    (if (straight? prev-piece)
      true)))

(defn long-straight? [index]
  (when (and (straight? (get-piece index))
             (straight? (get-piece (inc index)))
             (straight? (get-piece (inc (inc index)))))
    true))

(defn switched? [current-position]
  (let [lap (:lap current-position)
        piece-index (:pieceIndex (:piecePosition current-position))]
    (if ((keyword (str lap "-" piece-index)) @switch-tracker) true false)))

(defn get-piece-length [index lane]
  (let [piece (get-piece index)
        offset (:distanceFromCenter (get @lanes lane))]
    (if (:length piece)
      (:length piece)
      (if (switch? piece)
        (* (/ (Math/abs (:angle piece)) 360.0) (/ 44.0 7.0) (:radius piece))
        (if (> (:angle piece) 0)
          (* (/ (Math/abs (:angle piece)) 360.0) (/ 44.0 7.0) (- (:radius piece) offset))
          (* (/ (Math/abs (:angle piece)) 360.0) (/ 44.0 7.0) (+ (:radius piece) offset)))))))

(defn track-lengths [first-piece-index last-piece-index]
  (let [first-piece (get-piece first-piece-index)
        last-piece (get-piece last-piece-index)
        track-lanes @lanes]
    (if (= first-piece-index last-piece-index)
      (mapv (fn [lane] (get-piece-length first-piece-index (:index lane))) track-lanes)
      (mapv (fn [lane]
              (reduce + (map get-piece-length (range first-piece-index last-piece-index) (repeat (:index lane)))))
            track-lanes))))


(defn switch-optimum? [current-position]
  (let [piece-index (:pieceIndex (:piecePosition current-position))
        current-lane (:endLaneIndex (:lane (:piecePosition current-position)))
        next-switch (find-next-switch piece-index)
        track-lanes @lanes
        track-lengths-vec (track-lengths (inc (inc piece-index)) next-switch)
        left-lane (if (<= 0 (dec current-lane)) (dec current-lane) current-lane)
        right-lane (if (> (count track-lanes) (inc current-lane)) (inc current-lane) current-lane)
        left-lane-length (get track-lengths-vec left-lane)
        right-lane-length (get track-lengths-vec right-lane)
        current-lane-length (get track-lengths-vec current-lane)]
    (when (or (> current-lane-length left-lane-length)  (> current-lane-length right-lane-length))
      (if (> left-lane-length right-lane-length)
        "Right"
        "Left"))))

(defn velocity [current-position previous-position]
  (if (= (:pieceIndex (:piecePosition current-position)) (:pieceIndex (:piecePosition previous-position)))
    (- (:inPieceDistance (:piecePosition current-position)) (:inPieceDistance (:piecePosition previous-position)))
    (+ (:inPieceDistance (:piecePosition current-position)) (-
                                                             (get-piece-length (:pieceIndex (:piecePosition previous-position))
                                                                               (:endLaneIndex (:lane (:piecePosition previous-position))))
                                                             (:inPieceDistance (:piecePosition previous-position))))))

(defn check-throttle [throttle]
  (when (and (= throttle (:throttle (get @previous-positions 0))) (= throttle (:throttle (get @previous-positions 1))))
    true))

(defn big-turn? [piece-index]
  (let [current-piece (get-piece piece-index)
        next-piece (get-piece (inc  piece-index))]
    (if (and (turn? current-piece) (turn? next-piece))
      (if (> (reduce + (map (fn [piece] (Math/abs (:angle piece))) [current-piece next-piece])) 89)
        true
        false)
      false)))

(defn get-max-velocity [piece lane]
  (get (get @max-velocities lane) piece))

(defn get-acceleration [current-position]
  (let [current-velocity (velocity current-position (get @previous-positions 0))
        previous-velocity (velocity (get @previous-positions 0) (get @previous-positions 1))]
    (- current-velocity previous-velocity)))

(defn get-deceleration [current-position]
  (let [current-velocity (velocity current-position (get @previous-positions 0))
        previous-velocity (velocity (get @previous-positions 0) (get @previous-positions 1))]
    (- previous-velocity current-velocity)))

(defn set-maximum-velocities []
  (let [max-v (mapv (fn [lane]
                      (mapv (fn [piece]
                              (if (turn? piece)
                                (Math/sqrt (* @F-slip (- (:radius piece) (:distanceFromCenter lane))))
                                nil)) @pieces)) @lanes)]
    (reset! max-velocities max-v)
    (spit "/tmp/max-v" max-v)))


(defn straight-coeffs-available? []
  (when (and @throttle-coeff @decel-coeff)
    true))

(defn turn-coeffs-available? []
  (when @F-slip
    true))

(defn my-car? [car]
  (when (= @my-car (:color (:id car)))
    true))

(defn get-my-position [car-positions]
  (first (filter my-car? car-positions)))

(defn ticks-to-decelerate [current-velocity target-velocity accumulator]
  (if (> current-velocity target-velocity)
    (ticks-to-decelerate (- current-velocity (* @decel-coeff current-velocity)) target-velocity (inc accumulator))
    accumulator))

(defn match-velocity [msg]
  (let [current-position (get-my-position (:data msg))
        current-velocity (velocity current-position (get @previous-positions 0))
        current-piece-index (:pieceIndex (:piecePosition current-position))
        current-lap (:lap current-position)
        next-turn (find-next-turn current-piece-index)
        max-vel (or (get-max-v next-turn (:endLaneIndex (:lane (:piecePosition current-position))) current-lap) 100)]
    (if (> current-velocity 0)
      (if (straight-coeffs-available?)
        (if (turn-coeffs-available?)
          (if (and (switch? (get-piece (inc current-piece-index))) (not (switched? current-position)))
            (if-let [switch-direction (switch-optimum? current-position)]
              (do (swap! switch-tracker #(conj % {(keyword (str current-lap "-" current-piece-index)) true}))
                  {:switch switch-direction})
              {:throttle (/ (+ 0 (* current-velocity @decel-coeff)) @throttle-coeff)})
            (if (turn? (get-piece current-piece-index))
              (let [time-to-decelerate (ticks-to-decelerate current-velocity max-vel 0)
                    distance (- (get (track-lengths current-piece-index next-turn)
                                     (:endLaneIndex (:lane (:piecePosition current-position))))
                                (:inPieceDistance (:piecePosition current-position)))
                    ticks-left (Math/floor (/ distance current-velocity))]
                (if (<= ticks-left time-to-decelerate)
                  {:throttle 0.0}
                  (let [acceleration-max (- (get-max-v current-piece-index (:endLaneIndex (:lane (:piecePosition current-position))) current-lap) current-velocity)
                        acceleration-next (/ (- distance (* current-velocity ticks-left)) (- ticks-left 1))
                        throttle (/ (+ (min acceleration-max acceleration-next) (* current-velocity @decel-coeff)) @throttle-coeff)]
                    {:throttle throttle})))
              (let [time-to-decelerate (ticks-to-decelerate current-velocity max-vel 0)
                    distance (- (get (track-lengths current-piece-index next-turn)
                                     (:endLaneIndex (:lane (:piecePosition current-position))))
                                (:inPieceDistance (:piecePosition current-position)))
                    ticks-left (Math/floor (/ distance current-velocity))]
                (if (<= ticks-left time-to-decelerate)
                  {:throttle 0.0}
                  (let [acceleration (/ (- distance (* current-velocity ticks-left)) (- ticks-left 1))
                        throttle (/ (+ acceleration (* current-velocity @decel-coeff)) @throttle-coeff)]
                    (if (> throttle 1.0)
                      (if (and @turbo-available? (long-straight? current-piece-index))
                        {:turbo true}
                        {:throttle throttle})
                      {:throttle throttle}))))))
          (if (< 0 (:angle current-position))
            (let [piece (get @pieces current-piece-index)
                  B (Math/toDegrees (/ current-velocity (:radius piece)))
                  B-slip (- B (Math/abs (:angle current-position)))
                  V-thres (* (:radius piece) (Math/toRadians B-slip))
                  F-sl (/ (* V-thres V-thres) (:radius piece))]
              (reset! F-slip F-sl)
              (println "\n" F-sl "\n" @throttle-coeff "\n" @decel-coeff "\n")
              (set-maximum-velocities)
              (match-velocity msg))
            (if (turn? (get-piece current-piece-index))
              (if (= current-lap 0)
                {:throttle 0.6}
                (if (= current-lap 1)
                  {:throttle 0.8}
                  {:throttle 0.9}))
              {:throttle 0.4})))
        (let [p0 (get @previous-positions 1)
              p1 (get @previous-positions 0)
              p2 current-position
              v0 (velocity p1 p0)
              v1 (velocity p2 p1)]
          (println "\n"
                   (str "p0: " p0  "p1: " p1 "p2: " p2 "v0: " v0 "v1: " v1 "\n"))

          (when (and (not (= 0.0 (:inPieceDistance (:piecePosition p0)))) (= (:inPieceDistance (:piecePosition p1)) (:inPieceDistance (:piecePosition p0))) (= 0.0 v0) (< 0.0 (- v1 v0)))
            (reset! throttle-coeff (/ v1 1)))
          (when (and (not (= 0.0 (:inPieceDistance (:piecePosition p0)))) (< 0.0 v0) (< 0.0 (- v1 v0)))
            (reset! decel-coeff (/ (- (* @throttle-coeff (:throttle p1)) (- v1 v0)) v0)))
          {:throttle 1.0}))
      {:throttle 1.0})))


(defn driver [msg]
  (let [response (match-velocity msg)
        current-position (get-my-position (:data msg))]
    (if (:throttle response)
      (let [throttle (if @turbo-active?
                       (if (> (/ (:throttle response) (:turboFactor @turbo-available)) 1.0) 1.0
                           (if (< (/ (:throttle response) (:turboFactor @turbo-available)) 0.0) 0.0
                               (/ (:throttle response) (:turboFactor @turbo-available))))
                       (if (> (:throttle response) 1.0) 1.0
                           (if (< (:throttle response) 0.0) 0.0
                               (:throttle response))))]
        (swap! previous-positions #(apply conj [(assoc current-position :throttle throttle)] (drop-last %)))
        {:msgType "throttle" :data throttle :gameTick (:gameTick msg)})
      (if (:switch response)
        (do
          (swap! previous-positions #(apply conj [(assoc current-position :throttle 0.0)] (drop-last %)))
          {:msgType "switchLane" :data (:switch response) :gameTick (:gameTick msg)})
        (if (:turbo response)
          (do
            (swap! previous-positions #(apply conj [(assoc current-position :throttle 1.0)] (drop-last %)))
            (reset! turbo-available? false)
            {:msgType "turbo" :data "fly!" :gameTick (:gameTick msg)}))))))

(defmulti handle-msg :msgType)

(defmethod handle-msg "carPositions" [msg]
  (if (:gameTick msg)
    (driver msg)
    {:msgType "throttle" :data 1.0 :gameTick 0}))


(defmethod handle-msg "crash" [msg]
  {:msgType "ping" :data "ping" :gameTick 0})

(defmethod handle-msg "yourCar" [msg]
  (reset! my-car (:color (:data msg)))
  {:msgType "ping" :data "ping" :gameTick 0})

(defmethod handle-msg "gameInit" [msg]
  (reset! pieces (:pieces (:track (:race (:data msg)))))
  (reset! lanes (:lanes (:track (:race (:data msg)))))
  (reset! cars (:cars (:race (:data msg))))
  (reset! max-velocities (vec (take (count @pieces) (repeat 100))))
  (reset! race-session (:raceSession (:race (:data msg))))
  {:msgType "ping" :data "ping" :gameTick 0})

(defmethod handle-msg "gameStart" [msg]
  {:msgType "ping" :data "ping" :gameTick 0})

(defmethod handle-msg "turboAvailable" [msg]
  (reset! turbo-available (:data msg))
  (reset! turbo-available? true)
  {:msgType "ping" :data "ping" :gameTick 0})

(defmethod handle-msg "turboStart" [msg]
  (when (= @my-car (:color (:data msg)))
    (reset! turbo-active? true))
  {:msgType "ping" :data "ping" :gameTick 0})

(defmethod handle-msg "turboEnd" [msg]
  (when (= @my-car (:color (:data msg)))
    (reset! turbo-active? false))
  {:msgType "ping" :data "ping" :gameTick 0})

(defmethod handle-msg :default [msg]
  {:msgType "ping" :data "ping" :gameTick 0})

(defn log-msg [msg]
  (case (:msgType msg)
    "join" (println (str "Joined " msg))
    "gameStart" (println (str "Race started " msg))
    "gameEnd" (println "Race ended")
    "error" (println (str "ERROR: " (:data msg)))
    :noop))

(defn game-loop [channel]
  (let [msg (read-message channel)]
    (log-msg msg)
    (send-message channel (handle-msg msg))
    (recur channel)))

(defn -main[& [host port botname botkey]]
  (let [channel (connect-client-channel host (Integer/parseInt port))]
    (send-message channel {:msgType "join" :data {:name botname :key botkey} :carCount 3})
    (game-loop channel)))
